#pragma once
#include "IExamInterface.h"

class IExamInterface;
class Blackboard;

class Inventory
{

public:
	Inventory(Blackboard* pBlackboard);
	~Inventory();
	void Update(float dt);

	const bool AddItem(const ItemInfo &item);
	int CheckMetaData(const UINT &slot, const std::string &parameter);
	const bool RemoveItem(const UINT &slot);
	const bool UseItem(const UINT &slot);
	const bool DropItem(const UINT &slot);

	const ItemInfo GetItem(const UINT & slot);

	const UINT FindItem(const ItemInfo &info);
	const UINT FindItem(const eItemType &type);

	vector<Slot>* UpdateInventory();
	vector<Slot>* GetInventory();
	bool GunCooledDown();

	//variables
private:
	IExamInterface* m_pData = nullptr;
	bool slotInUse[5] = { false };
	vector<Slot> m_CurrentInventory;

	float m_GunCooldown = 0.f;
	float m_GunMaxCooldown = 0.01f;
	bool m_GunCooledDown = true;
};

