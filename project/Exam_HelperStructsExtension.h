#pragma once
#include "Exam_HelperStructs.h"

namespace BOARD
{
	const std::string INTERFACE = "Interface";
	const std::string OUTPUT_BEHAVIOUR = "CurrentBehaviour";
	const std::string TARGET = "Target";
	const std::string INVENTORY = "Inventory";
	const std::string ENTITIES = "Entities";
	const std::string HOUSES = "Houses";
	const std::string HOUSES_EXT = "Houses Extended";
	const std::string WAYPOINTS = "Waypoints";
	const std::string RUNNING = "RunMode";
	const std::string ALLIGNED = "Alligned";
	const std::string AIMING = "IsAiming";
}

struct Slot
{
	UINT slotNumber = -1;
	ItemInfo item;
};

struct HousesVisited : HouseInfo
{
	vector<Elite::Vector2> entrances;
	bool wayPointsCreated = false;
	bool houseCompleted = false;
};

bool operator== (const HouseInfo &hi, const HousesVisited &hv)
{
	return hi.Center == hv.Center;
}