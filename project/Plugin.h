#pragma once
#include "IExamPlugin.h"
#include "Exam_HelperStructs.h"
#include "SteeringBehaviours.h"

class IBaseInterface;
class IExamInterface;
class Inventory;
class Blackboard;
class BehaviourManager;

class Plugin :public IExamPlugin
{
public:
	Plugin() {};
	virtual ~Plugin() {};

	void Initialize(IBaseInterface* pInterface, PluginInfo& info) override;
	void DllInit() override;
	void DllShutdown() override;

	void InitGameDebugParams(GameDebugParams& params) override;
	void ProcessEvents(const SDL_Event& e) override;

	SteeringPlugin_Output UpdateSteering(float dt) override;
	void Render(float dt) const override;

private:
	//Interface, used to request data from/perform actions with the AI Framework
	IExamInterface* m_pInterface = nullptr;
	vector<HouseInfo> GetHousesInFOV() const;
	vector<EntityInfo> GetEntitiesInFOV() const;

	Elite::Vector2 m_Target = { 0,0 };
	bool m_CanRun = false; //Demo purpose
	bool m_GrabItem = false; //Demo purpose
	bool m_UseItem = false; //Demo purpose
	bool m_RemoveItem = false; //Demo purpose
	bool m_DropItem = false; //Demo purpose
	float m_AngSpeed = 0.f; //Demo purpose

	Blackboard* m_pBlackboard = nullptr;
	BehaviourManager* m_pBehaviourController = nullptr;

	Elite::Vector2* m_pTarget = nullptr;
	SteeringBehaviours::ISteeringBehaviour* m_pCurrentBehaviour = new SteeringBehaviours::Wander();
	
	vector<HouseInfo> m_VecHouseInfo = {};
	vector<HousesVisited> m_VecHouseVisited = {};
	vector<EntityInfo> m_VecEntityInfo = {};
	bool m_Running = false;
};

//ENTRY
//This is the first function that is called by the host program
//The plugin returned by this function is also the plugin used by the host program
extern "C"
{
	__declspec (dllexport) IPluginBase* Register()
	{
		return new Plugin();
	}
}