#pragma once
#include "stdafx.h"
#include "IExamInterface.h"
#include "BehaviorTree.h"

//parameters
Elite::Vector2 BorderOffset = { 8,8 };

//function declaration for sake of structure
inline bool IsNewHouse(Blackboard* pBlackboard);

#pragma region Helpers
//sort entities in FOV by distance (closest to furthest)
//inline void SortEntitiesByDistance(Blackboard *pBlackboard)
//{
//	vector<EntityInfo>* pEntitiesInFOV = nullptr;
//	IExamInterface* pInterface = nullptr;
//
//	if (pBlackboard->GetData(BOARD::ENTITIES, pEntitiesInFOV)
//		&& pBlackboard->GetData(BOARD::INTERFACE, pInterface))
//	{
//		vector<EntityInfo> entitiesInFOV = *pEntitiesInFOV;
//		if (entitiesInFOV.size() > 1)
//		{
//			std::sort(entitiesInFOV.begin(), entitiesInFOV.end(), [pInterface](const EntityInfo &a, const EntityInfo &b)
//			{
//				return Elite::Distance(a.Location, pInterface->Agent_GetInfo().Position) <
//					Elite::Distance(b.Location, pInterface->Agent_GetInfo().Position);
//			});
//
//			pEntitiesInFOV = &entitiesInFOV;
//			pBlackboard->ChangeData(BOARD::ENTITIES, pEntitiesInFOV);
//		}
//	}
//}

inline void AddHouseEntrances(Blackboard *pBlackboard)
{
	vector<HousesVisited>* hiVisited = nullptr;
	vector<HouseInfo>* hi = nullptr;
	IExamInterface* pInterface = nullptr;

	if (pBlackboard->GetData(BOARD::HOUSES_EXT, hiVisited)
		&& pBlackboard->GetData(BOARD::HOUSES, hi)
		&& pBlackboard->GetData(BOARD::INTERFACE, pInterface))
	{
		if ((*hi).size() <= 0 || !pInterface->Agent_GetInfo().IsInHouse) return;

		for (size_t i = 0; i < hi->size(); i++)
		{
			auto visitedExistsIterator = std::find_if(hiVisited->begin(), hiVisited->end(), [&](const HousesVisited& info) { return hi->at(i).Center == info.Center; });
			if (visitedExistsIterator != hiVisited->end())
			{
				UINT visitedExistsId = visitedExistsIterator - hiVisited->begin();
				if (hiVisited->at(visitedExistsId).entrances.size() == 0)
				{
					hiVisited->at(visitedExistsId).entrances.push_back(pInterface->Agent_GetInfo().Position);
				}

				auto entranceExistsIterator = std::find_if(hiVisited->at(visitedExistsId).entrances.begin(), hiVisited->at(visitedExistsId).entrances.end(), [&](const Elite::Vector2 &pos) {
					return Elite::Distance(pInterface->Agent_GetInfo().Position, hiVisited->at(visitedExistsId).Center)
						< Elite::Distance(pos, hiVisited->at(visitedExistsId).Center) + 5; });

				if (entranceExistsIterator == (hiVisited->at(visitedExistsId).entrances.end()))
				{
					hiVisited->at(visitedExistsId).entrances.push_back(pInterface->Agent_GetInfo().Position);
				}
			}
			else
			{
				HousesVisited house;
				house.Center = hi->at(i).Center;
				house.Size = hi->at(i).Size;

				hiVisited->push_back(house);
			}
		}
	}
}

inline HousesVisited* FindVisitedHouseData(HouseInfo* hi, vector<HousesVisited>* hv)
{
	auto houseVisited = new HousesVisited();

	auto it = std::find_if(hv->begin(), hv->end(), [&](const HousesVisited &house) {
		return house.Center == hi->Center;
	});

	if (it != hv->end()) return &hv->at(it - hv->begin());
	else return new HousesVisited();
}

inline const vector<Elite::Vector2> WaypointCurrentHouse(Blackboard *pBlackboard)
{
	vector<HouseInfo>* pHouses = nullptr;
	vector<HousesVisited>* pHousesVisited = nullptr;
	IExamInterface* pInterface = nullptr;
	vector<Elite::Vector2>* pWaypoints = nullptr;

	if (pBlackboard->GetData(BOARD::HOUSES, pHouses)
		&& pBlackboard->GetData(BOARD::INTERFACE, pInterface)
		&& pBlackboard->GetData(BOARD::HOUSES_EXT, pHousesVisited)
		&& pBlackboard->GetData(BOARD::WAYPOINTS, pWaypoints))
	{
		if (pWaypoints->size() != 0) return *pWaypoints;

		if (pHouses->size() != 0 && pHousesVisited->size() != 0)
		{
			vector<Elite::Vector2> out;

			Elite::Vector2 agentPos = pInterface->Agent_GetInfo().Position;
			HouseInfo houseInfo = pHouses->at(0);

			HousesVisited* houseVisited = FindVisitedHouseData(&houseInfo, pHousesVisited);
			if (!houseVisited->houseCompleted && !houseVisited->wayPointsCreated)
			{
				Elite::Vector2 halfSize = houseInfo.Size / 2;

				//add waypoints based on data from house
				out.push_back(houseInfo.Center + Elite::Vector2(-halfSize.x, halfSize.y) + Elite::Vector2(BorderOffset.x, -BorderOffset.y)); //topLeft
				out.push_back(houseInfo.Center + Elite::Vector2(halfSize.x, halfSize.y) + Elite::Vector2(-BorderOffset.x, -BorderOffset.y)); //topRight
				out.push_back(houseInfo.Center + Elite::Vector2(-halfSize.x, -halfSize.y) + Elite::Vector2(BorderOffset.x, BorderOffset.y)); //bottomLeft
				out.push_back(houseInfo.Center + Elite::Vector2(halfSize.x, -halfSize.y) + Elite::Vector2(-BorderOffset.x, BorderOffset.y)); //bottomRight
				return out;
			}
		}
	}
	return vector<Elite::Vector2>();
}

inline void OrderWaypointsByDistance(Blackboard *pBlackboard)
{
	vector<Elite::Vector2>* pWaypoints = nullptr;
	vector<HousesVisited>* pHV = nullptr;
	vector<HouseInfo>* pHouses = nullptr;
	IExamInterface* pInterface = nullptr;

	if (pBlackboard->GetData(BOARD::WAYPOINTS, pWaypoints)
		&& pBlackboard->GetData(BOARD::HOUSES_EXT, pHV)
		&& pBlackboard->GetData(BOARD::HOUSES, pHouses)
		&& pBlackboard->GetData(BOARD::INTERFACE, pInterface))
	{
		if (pHouses->size() == 0) return;
		HousesVisited* hv = FindVisitedHouseData(&(pHouses->at(0)), pHV);

		if (pWaypoints->size() == 0 || hv->wayPointsCreated) return;

		//sort the data by distance
		std::sort(pWaypoints->begin(), pWaypoints->end(), [&](const Elite::Vector2 &wp, const Elite::Vector2 &wp2) {
			return Elite::Distance(wp, pInterface->Agent_GetInfo().Position) < Elite::Distance(wp2, pInterface->Agent_GetInfo().Position);
		});

		//add initial waypoint to get back to starting point
		pWaypoints->push_back(pWaypoints->at(0));

		//add checkpoint to exit house and continue
		pWaypoints->push_back(pInterface->World_GetCheckpointLocation());

		//points created, dont do this again for current house
		hv->wayPointsCreated = true;
	}
}

#pragma endregion

#pragma region Behaviours

#pragma region Conditionals
inline bool ItemInGrabRange(Blackboard *pBlackboard)
{
	IExamInterface* pInterface = nullptr;
	vector<EntityInfo>* pEntityInfo = nullptr;

	if (pBlackboard->GetData(BOARD::ENTITIES, pEntityInfo)
		&& pBlackboard->GetData(BOARD::INTERFACE, pInterface))
	{
		for (size_t i = 0; i < pEntityInfo->size(); i++)
		{
			if (pEntityInfo->at(i).Type == eEntityType::ITEM)
			{
				if (Elite::Distance(pInterface->Agent_GetInfo().Position, pEntityInfo->at(i).Location)
					< pInterface->Agent_GetInfo().GrabRange)
				{
					return true;
				}
			}
		}
	}
	return false;
}

inline bool HouseNotVisited(Blackboard *pBlackboard)
{
	vector<HousesVisited>* pHV = nullptr;
	vector<HouseInfo>* pHI = nullptr;

	if (pBlackboard->GetData(BOARD::HOUSES, pHI)
		&& pBlackboard->GetData(BOARD::HOUSES_EXT, pHV))
	{
		HousesVisited* houseVisited = FindVisitedHouseData(&pHI->at(0), pHV);
		if (!houseVisited->houseCompleted)
		{
			return true;
		}
	}
	return false;
}

inline bool CheckpointInCurrentHouse(Blackboard *pBlackboard)
{
	IExamInterface* pInterface = nullptr;
	vector<HouseInfo>* pHouses = nullptr;

	if (pBlackboard->GetData(BOARD::INTERFACE, pInterface)
		&& pBlackboard->GetData(BOARD::HOUSES, pHouses))
	{
		auto checkpointPos = pInterface->World_GetCheckpointLocation();
		auto agentPos = pInterface->Agent_GetInfo().Position;
		auto currHouse = pHouses->at(0);

		if (checkpointPos.x > currHouse.Center.x - currHouse.Size.x && checkpointPos.x < currHouse.Center.x + currHouse.Size.x
			&& checkpointPos.y > currHouse.Center.y - currHouse.Size.y && checkpointPos.y < currHouse.Center.y + currHouse.Size.y)
		{
			return true;
		}
	}
	return false;
}

inline bool HasWeaponAndAmmo(Blackboard *pBlackboard)
{
	Inventory* pInventory = nullptr;

	if (pBlackboard->GetData(BOARD::INVENTORY, pInventory))
	{
		int slot = pInventory->FindItem(eItemType::PISTOL);
		if (slot != -1)
		{
			int ammo = pInventory->CheckMetaData(slot, "ammo");
			if (ammo != 0) return true;
		}
	}
	return false;
}

inline bool HouseWaypointExists(Blackboard* pBlackboard)
{
	vector<Elite::Vector2>* pWaypoints;

	if (pBlackboard->GetData(BOARD::WAYPOINTS, pWaypoints))
	{
		if (pWaypoints->size() > 0)
		{
			return true;
		}
	}
	return false;
}

inline bool NavigationCompleted(Blackboard *pBlackboard)
{
	vector<Elite::Vector2>* pWaypoints;
	IExamInterface* pInterface = nullptr;
	vector<HouseInfo>* pHI = nullptr;
	vector<HousesVisited>* pHV = nullptr;

	if (pBlackboard->GetData(BOARD::WAYPOINTS, pWaypoints)
		&& pBlackboard->GetData(BOARD::INTERFACE, pInterface)
		&& pBlackboard->GetData(BOARD::HOUSES, pHI)
		&& pBlackboard->GetData(BOARD::HOUSES_EXT, pHV))
	{
		if (pWaypoints->size() == 1 && FindVisitedHouseData(&pHI->at(0), pHV)->wayPointsCreated)
		{
			return true;
		}
	}
	return false;
}

inline bool FOVEmpty(Blackboard *pBlackboard)
{
	vector<EntityInfo>* pEntities = nullptr;

	if (pBlackboard->GetData(BOARD::ENTITIES, pEntities))
	{
		if (pEntities->size() == 0)
		{
			return true;
		}
	}
	return false;
}

inline bool ItemInFOV(Blackboard* pBlackboard)
{
	vector<EntityInfo>* pEntities = nullptr;

	if (pBlackboard->GetData(BOARD::ENTITIES, pEntities))
	{
		for (size_t i = 0; i < pEntities->size(); i++)
		{
			if (pEntities->at(i).Type == eEntityType::ITEM)
			{
				return true;
			}
		}
	}
	return false;
}

inline bool NoItemInFOV(Blackboard *pBlackboard)
{
	return !ItemInFOV(pBlackboard);
}

inline bool EnemyBiting(Blackboard *pBlackboard)
{
	IExamInterface* pInterface = nullptr;
	vector<EntityInfo>* pEntities = nullptr;

	if (pBlackboard->GetData(BOARD::INTERFACE, pInterface)
		&& pBlackboard->GetData(BOARD::ENTITIES, pEntities))
	{
		if (pInterface->Agent_GetInfo().Bitten)
		{
			return true;
		}
	}
	return false;
}

inline bool EnemyInFOV(Blackboard *pBlackboard)
{
	vector<EntityInfo>* pEntities = nullptr;

	if (pBlackboard->GetData(BOARD::ENTITIES, pEntities))
	{
		for (size_t i = 0; i < pEntities->size(); i++)
		{
			if (pEntities->at(i).Type == eEntityType::ENEMY)
			{
				return true;
			}
		}
	}
	return false;
}

inline bool HouseInFOV(Blackboard* pBlackboard)
{
	IExamInterface* pInterface = nullptr;
	vector<HouseInfo>* pHouses = nullptr;

	if (pBlackboard->GetData(BOARD::INTERFACE, pInterface) && pBlackboard->GetData(BOARD::HOUSES, pHouses))
	{
		if (pHouses->size() > 0 || pInterface->Agent_GetInfo().IsInHouse)
		{
			return true;
		}
	}
	return false;
}

inline bool NoHouseInFOV(Blackboard *pBlackboard)
{
	return !HouseInFOV(pBlackboard);
}

inline bool AgentInHouse(Blackboard* pBlackboard)
{
	IExamInterface* pInterface = nullptr;

	if (pBlackboard->GetData(BOARD::INTERFACE, pInterface))
	{
		if (pInterface->Agent_GetInfo().IsInHouse)
		{
			return true;
		}
	}
	return false;
}

inline bool AgentNotInHouse(Blackboard *pBlackboard)
{
	return !AgentInHouse(pBlackboard);
}

inline bool IsNotLooking(Blackboard* pBlackboard)
{
	bool* pIsAiming = nullptr;

	if (pBlackboard->GetData(BOARD::AIMING, pIsAiming))
	{
		return !*pIsAiming;
	}
	return false;
}

inline bool EnemyInFront(Blackboard *pBlackboard)
{
	bool* pAlligned = nullptr;

	if (pBlackboard->GetData(BOARD::ALLIGNED, pAlligned))
	{
		return *pAlligned;
	}
	return false;
}
#pragma endregion

#pragma region Actions

inline BehaviorState TargetHouseWaypoints(Blackboard *pBlackboard)
{
	Elite::Vector2* pTarget = nullptr;
	vector<Elite::Vector2>* pWaypoints = nullptr;
	IExamInterface* pInterface = nullptr;

	if (pBlackboard->GetData(BOARD::TARGET, pTarget)
		&& pBlackboard->GetData(BOARD::WAYPOINTS, pWaypoints)
		&& pBlackboard->GetData(BOARD::INTERFACE, pInterface))
	{
		if (pWaypoints->size() != 0)
		{
			if (Elite::Distance(pInterface->Agent_GetInfo().Position, pWaypoints->at(0)) > pInterface->Agent_GetInfo().GrabRange)
			{
				*pTarget = pWaypoints->at(0);
				return Running;
			}
			else
			{
				pWaypoints->erase(pWaypoints->begin());
			}
		}
		return Success;
	}
	return Failure;
}

inline BehaviorState ClearWaypoints(Blackboard *pBlackboard)
{
	vector<Elite::Vector2>* pWaypoints;

	if (pBlackboard->GetData(BOARD::WAYPOINTS, pWaypoints))
	{
		pWaypoints->clear();
		return Success;
	}
	return Failure;
}

inline BehaviorState GenerateWaypoints(Blackboard *pBlackboard)
{
	vector<Elite::Vector2>* pWaypoints = nullptr;
	vector<HouseInfo>* pHI = nullptr;
	vector<HousesVisited>* pHV = nullptr;

	if (pBlackboard->GetData(BOARD::WAYPOINTS, pWaypoints)
		&& pBlackboard->GetData(BOARD::HOUSES, pHI)
		&& pBlackboard->GetData(BOARD::HOUSES_EXT, pHV))
	{
		if (!FindVisitedHouseData(&pHI->at(0), pHV)->wayPointsCreated)
		{
			*pWaypoints = WaypointCurrentHouse(pBlackboard);
			OrderWaypointsByDistance(pBlackboard);
		}
		return Success;
	}
	return Failure;
}

inline BehaviorState SetClosestItemAsTarget(Blackboard* pBlackboard)
{
	Elite::Vector2* pTarget = nullptr;
	vector<EntityInfo>* pEntities = nullptr;

	if (pBlackboard->GetData(BOARD::TARGET, pTarget) 
		&& pBlackboard->GetData(BOARD::ENTITIES, pEntities))
	{
		for (size_t i = 0; i < pEntities->size(); i++)
		{
			if (pEntities->at(i).Type == eEntityType::ITEM)
			{
				*pTarget = pEntities->at(i).Location;
				return Success;
			}
		}
	}
	return Failure;
}

inline BehaviorState PickUpItem(Blackboard* pBlackboard)
{
	Inventory* pInventory = nullptr;
	IExamInterface* pInterface = nullptr;

	if (pBlackboard->GetData(BOARD::INVENTORY, pInventory) && pBlackboard->GetData(BOARD::INTERFACE, pInterface))
	{
		ItemInfo item;
		if (pInterface->Item_Grab({}, item))
		{
			if(pInventory->AddItem(item))
			{
				return Success;
			}
		}
	}
	return Failure;
}

inline BehaviorState TargetCheckpoint(Blackboard *pBlackboard)
{
	IExamInterface* pInterface = nullptr;
	Elite::Vector2* pTarget = nullptr;
	vector<Elite::Vector2>* pWaypoints = nullptr;

	if (pBlackboard->GetData(BOARD::INTERFACE, pInterface) 
		&& pBlackboard->GetData(BOARD::TARGET, pTarget)
		&& pBlackboard->GetData(BOARD::WAYPOINTS, pWaypoints))
	{
		*pTarget = pInterface->World_GetCheckpointLocation();
		return Success;
	}
	return Failure;
}

inline BehaviorState TargetHouseCenter(Blackboard *pBlackboard)
{
	vector<HouseInfo>* pHouses = nullptr;
	Elite::Vector2* pTarget = nullptr;
	IExamInterface* pInterface = nullptr;

	if (pBlackboard->GetData(BOARD::HOUSES, pHouses) 
		&& pBlackboard->GetData(BOARD::TARGET, pTarget)
		&& pBlackboard->GetData(BOARD::INTERFACE, pInterface))
	{
		if (pHouses->size() > 0)
		{
			if (!pInterface->Agent_GetInfo().IsInHouse)
			{
				*pTarget = pHouses->front().Center;
				return Running;
			}
			return Success;
		}
	}
	return Failure;
}

inline BehaviorState ExitHouse(Blackboard* pBlackboard)
{
	vector<HouseInfo>* pHouses = nullptr;
	vector<HousesVisited>* pHV = nullptr;
	IExamInterface *pInterface = nullptr;
	vector<Elite::Vector2>* pWaypoints = nullptr;
	Elite::Vector2* pTarget = nullptr;
	
	if (pBlackboard->GetData(BOARD::HOUSES, pHouses)
		&& pBlackboard->GetData(BOARD::HOUSES_EXT, pHV)
		&& pBlackboard->GetData(BOARD::INTERFACE, pInterface)
		&& pBlackboard->GetData(BOARD::WAYPOINTS, pWaypoints)
		&& pBlackboard->GetData(BOARD::TARGET, pTarget))
	{
		FindVisitedHouseData(&pHouses->at(0), pHV)->houseCompleted = true;
		*pTarget = pInterface->World_GetCheckpointLocation();
		return Success;
	}
	return Failure;
}

inline BehaviorState SetEnemyAsTarget(Blackboard* pBlackboard)
{
	Elite::Vector2* pTarget = nullptr;
	vector<EntityInfo>* pEntities = nullptr;
	bool* pIsAiming = nullptr;

	if (pBlackboard->GetData(BOARD::TARGET, pTarget)
		&& pBlackboard->GetData(BOARD::ENTITIES, pEntities)
		&& pBlackboard->GetData(BOARD::AIMING, pIsAiming))
	{
		for (size_t i = 0; i < pEntities->size(); i++)
		{
			if (pEntities->at(i).Type == eEntityType::ENEMY)
			{
				*pTarget = pEntities->at(i).Location;
				*pIsAiming = true;
				return Success;
			}
		}
	}
	return Failure;
}

inline BehaviorState ShootAtEnemy(Blackboard *pBlackboard)
{
	Inventory* pInventory = nullptr;
	bool* pIsAiming = nullptr;

	if (pBlackboard->GetData(BOARD::INVENTORY, pInventory)
		&& pBlackboard->GetData(BOARD::AIMING, pIsAiming))
	{
		UINT slotNumber = pInventory->FindItem(eItemType::PISTOL);
		if (slotNumber != -1 && pInventory->GunCooledDown())
		{
			pInventory->UseItem(slotNumber);
			*pIsAiming = false;
			return Success;
		}
	}
	return Failure;
}

inline BehaviorState InventoryManagement(Blackboard *pBlackboard)
{
	Inventory* pInventory = nullptr;
	IExamInterface* pInterface = nullptr;

	if (pBlackboard->GetData(BOARD::INVENTORY, pInventory)
		&& pBlackboard->GetData(BOARD::INTERFACE, pInterface))
	{
		AgentInfo pAI = pInterface->Agent_GetInfo();

		int slot = pInventory->FindItem(eItemType::MEDKIT);
		if (slot != -1)
		{
			int health = pInventory->CheckMetaData(slot, "health");
			if(pAI.Health < 2) pInventory->UseItem(slot);
			if (health == 0) pInventory->RemoveItem(slot);
		}

		slot = pInventory->FindItem(eItemType::FOOD);
		if (slot != -1)
		{
			int energy = pInventory->CheckMetaData(slot, "energy");
			if (pAI.Energy < 2) pInventory->UseItem(slot);
			if (energy == 0) pInventory->RemoveItem(slot);
		}

		slot = pInventory->FindItem(eItemType::PISTOL);
		if (slot != -1)
		{
			int ammo = pInventory->CheckMetaData(slot, "ammo");
			if (ammo == 0) pInventory->RemoveItem(slot);
		}

		pInventory->UpdateInventory();
		return Success;
	}
	return Failure;
}

inline BehaviorState SetRunning(Blackboard *pBlackboard)
{
	bool* pRun = nullptr;

	if (pBlackboard->GetData(BOARD::RUNNING, pRun))
	{
		*pRun = true;
		return Success;
	}
	return Failure;
}

inline BehaviorState SetLookat(Blackboard *pBlackboard)
{
	SteeringBehaviours::ISteeringBehaviour* pLook = new SteeringBehaviours::LookAt();
	if (pBlackboard->ChangeData(BOARD::OUTPUT_BEHAVIOUR, pLook))
	{
		return Success;
	}
	return Failure;
}

inline BehaviorState SetWander(Blackboard* pBlackboard)
{
	SteeringBehaviours::ISteeringBehaviour* pWander = new SteeringBehaviours::Wander();
	if (pBlackboard->ChangeData(BOARD::OUTPUT_BEHAVIOUR, pWander))
	{
		return Success;
	}
	return Failure;
}

inline BehaviorState SetSeek(Blackboard *pBlackboard)
{
	SteeringBehaviours::ISteeringBehaviour* pSeek = new SteeringBehaviours::Seek();
	if (pBlackboard->ChangeData(BOARD::OUTPUT_BEHAVIOUR, pSeek))
	{
		return Success;
	}
	return Failure;
}
#pragma endregion

#pragma endregion