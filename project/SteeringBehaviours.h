#pragma once
#include "stdafx.h"
#include "IExamInterface.h"

class Blackboard;

namespace SteeringBehaviours
{
	class ISteeringBehaviour
	{
	public:

		ISteeringBehaviour() {}
		virtual ~ISteeringBehaviour() {}

		virtual SteeringPlugin_Output CalculateSteering(Blackboard* pBlackboard) = 0;

		template<class T, typename std::enable_if<std::is_base_of<ISteeringBehaviour, T>::value>::type* = nullptr>
		T* As()
		{
			return static_cast<T*>(this);
		}
	};

	class Seek : public ISteeringBehaviour
	{
	public:

		Seek() {};
		virtual ~Seek() {};

		SteeringPlugin_Output CalculateSteering(Blackboard* pBlackboard) override;
	};

	class Wander : public Seek
	{
	public:

		Wander() {};
		virtual ~Wander() {};

		SteeringPlugin_Output CalculateSteering(Blackboard* pBlackboard) override;

	protected:
		float m_Offset = 10.f;
		float m_Radius = 8.f;
		float m_AngleChange = Elite::ToRadians(30);
		float m_WanderAngle = 0.f;
	};

	class LookAt : public ISteeringBehaviour
	{
	public:
		LookAt() {};
		virtual ~LookAt() {};

		SteeringPlugin_Output CalculateSteering(Blackboard* pBlackboard) override;
	};
}