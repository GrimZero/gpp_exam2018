#include "stdafx.h"
#include "BehaviourManager.h"
#include "SteeringBehaviours.h"
#include "Inventory.h"
#include "Behaviours.h"

BehaviourManager::BehaviourManager(Blackboard *pBlackboard)
{
	m_pInventory = new Inventory(pBlackboard);

	pBlackboard->GetData(BOARD::INTERFACE, m_pInterface);

	pBlackboard->AddData(BOARD::INVENTORY, m_pInventory);
	pBlackboard->AddData(BOARD::WAYPOINTS, &m_HouseCheckpoints);
	pBlackboard->AddData(BOARD::ALLIGNED, &m_Alligned);
	pBlackboard->AddData(BOARD::AIMING, &m_IsAiming);

	m_pBehaviorTree = new BehaviorTree(pBlackboard,
		new BehaviorSequence({
			new BehaviorAction(InventoryManagement),
			new BehaviorAction(SetRunning),
			new BehaviorSelector({
				new BehaviorSequence({
					new BehaviorConditional(HouseWaypointExists),
					new BehaviorConditional(AgentNotInHouse),
					new BehaviorAction(ClearWaypoints)
				}),
				new BehaviorSelector({
					new BehaviorSelector({
						new BehaviorSequence({
							new BehaviorConditional(HasWeaponAndAmmo),
							new BehaviorConditional(EnemyInFOV),
							new BehaviorAction(SetEnemyAsTarget),
							new BehaviorAction(SetLookat),
							new BehaviorConditional(EnemyInFront),
							new BehaviorAction(ShootAtEnemy)
							}),
					}),
					new BehaviorSequence({
						new BehaviorConditional(HouseInFOV),
						new BehaviorSelector({
							new BehaviorSequence({
								new BehaviorConditional(ItemInFOV),
								new BehaviorAction(SetClosestItemAsTarget),
								new BehaviorConditional(ItemInGrabRange),
								new BehaviorAction(PickUpItem)
								}),
							new BehaviorSequence({
								new BehaviorConditional(FOVEmpty),
								new BehaviorSelector({
									new BehaviorSequence({
										new BehaviorConditional(CheckpointInCurrentHouse),
										new BehaviorAction(TargetCheckpoint),
										new BehaviorAction(SetSeek)
										}),
									new BehaviorSequence({
										new BehaviorConditional(HouseNotVisited),
										new BehaviorAction(TargetHouseCenter),
										new BehaviorAction(GenerateWaypoints),
										new BehaviorAction(TargetHouseWaypoints),
										new BehaviorAction(SetSeek),
										new BehaviorConditional(NavigationCompleted),
										new BehaviorAction(ExitHouse)
										}),
									new BehaviorSequence({
										new BehaviorAction(TargetCheckpoint),
										new BehaviorAction(SetSeek)
										})
									})
								})
							})
						}),
					new BehaviorSequence({
						new BehaviorConditional(NoHouseInFOV),
						new BehaviorConditional(IsNotLooking),
						new BehaviorConditional(AgentNotInHouse),
						new BehaviorAction(TargetCheckpoint),
						new BehaviorAction(SetSeek)
						})
					})
				})
			})
		);
}

BehaviourManager::~BehaviourManager()
{
	SAFE_DELETE(m_pBehaviorTree);
	SAFE_DELETE(m_pInventory);

	m_HouseCheckpoints.clear();	
	m_Inventory.clear();
}

SteeringPlugin_Output BehaviourManager::ExecuteBehaviourTree(Blackboard *pBlackboard)
{
	//variable setup
	SteeringBehaviours::ISteeringBehaviour* pOutputBehaviour = nullptr;
	pBlackboard->GetData(BOARD::OUTPUT_BEHAVIOUR, pOutputBehaviour);

	//check items in the inventory
	m_Inventory = *m_pInventory->UpdateInventory();

	//add house entrances when encountered / check every frame (behaviourtree didnt track all)
	AddHouseEntrances(pBlackboard);
	
	//tree update
	if(m_pBehaviorTree) m_pBehaviorTree->Update();

	//Calculate steering
	pBlackboard->GetData(BOARD::OUTPUT_BEHAVIOUR, pOutputBehaviour);
	return pOutputBehaviour->CalculateSteering(pBlackboard);
}