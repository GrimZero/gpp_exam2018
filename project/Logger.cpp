#include "stdafx.h"
#include "Logger.h"

Logger::Logger() { }
Logger::~Logger() { }

void Logger::LogData(Level level, string message)
{
	switch (level)
	{
	case Logger::None:
		std::cout << message << std::endl;
		break;
	case Logger::Warning:
		LogWarning(message);
		break;
	case Logger::Error:
		LogError(message);
		break;
	default:
		break;
	}
}

void Logger::LogWarning(string message)
{
	std::cout << "[WARNING]" << " " + message << std::endl;
}

void Logger::LogError(string message)
{
	std::cout << "[ERROR]" << " " + message << std::endl;
}
