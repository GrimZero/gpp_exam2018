#include "stdafx.h"
#include "Plugin.h"
#include "IExamInterface.h"
#include "Inventory.h"
#include "Blackboard.h"
#include "SteeringBehaviours.h"
#include "BehaviourManager.h"

//Called only once, during initialization
void Plugin::Initialize(IBaseInterface* pInterface, PluginInfo& info)
{
	//ImGui info
	info.BotName = "Android";
	info.Student_FirstName = "Torben";
	info.Student_LastName = "Van Assche";
	info.Student_Class = "2DAE5";

	//create blackboard
	m_pBlackboard = new Blackboard();

	//get the interface and save it to the blackboard
	m_pInterface = static_cast<IExamInterface*>(pInterface);
	m_pBlackboard->AddData(INTERFACE, m_pInterface);

	//save addresses to data in blackboard 
	m_pBlackboard->AddData(TARGET, &m_Target);
	m_pBlackboard->AddData(BOARD::ENTITIES, &m_VecEntityInfo);
	m_pBlackboard->AddData(BOARD::HOUSES, &m_VecHouseInfo);
	m_pBlackboard->AddData(BOARD::HOUSES_EXT, &m_VecHouseVisited);
	m_pBlackboard->AddData(BOARD::RUNNING, &m_Running);

	//Steering
	m_pBlackboard->AddData(OUTPUT_BEHAVIOUR, m_pCurrentBehaviour);

	//Initialize processing class
	m_pBehaviourController = new BehaviourManager(m_pBlackboard);
}

void Plugin::DllInit()
{
}

//Called only once
void Plugin::DllShutdown()
{
	SAFE_DELETE(m_pBehaviourController);
	SAFE_DELETE(m_pTarget);
	SAFE_DELETE(m_pCurrentBehaviour);
	SAFE_DELETE(m_pBlackboard);

	m_VecHouseInfo.clear();
	m_VecHouseVisited.clear();
	m_VecEntityInfo.clear();
}

//Called only once, during initialization
void Plugin::InitGameDebugParams(GameDebugParams& params)
{
	params.AutoFollowCam = true; //Automatically follow the AI? (Default = true)
	params.RenderUI = true; //Render the IMGUI Panel? (Default = true)
	params.SpawnEnemies = true; //Do you want to spawn enemies? (Default = true)
	params.EnemyCount = 20; //How many enemies? (Default = 20)
	params.GodMode = false; //GodMode > You can't die, can be usefull to inspect certain behaviours (Default = false)
							//params.LevelFile = "LevelTwo.gppl";
	params.AutoGrabClosestItem = true; //A call to Item_Grab(...) returns the closest item that can be grabbed. (EntityInfo argument is ignored)
	params.OverrideDifficulty = false; //Override Difficulty?
	params.Difficulty = 1.f; //Difficulty Override: 0 > 1 (Overshoot is possible, >1)
}

//Only Active in DEBUG Mode
void Plugin::ProcessEvents(const SDL_Event& e)
{
	//Demo Event Code
	//In the end your AI should be able to walk around without external input
	switch (e.type)
	{
	case SDL_MOUSEBUTTONUP:
	{
		if (e.button.button == SDL_BUTTON_LEFT)
		{
			int x, y;
			SDL_GetMouseState(&x, &y);
			const Elite::Vector2 pos = Elite::Vector2(static_cast<float>(x), static_cast<float>(y));
			m_Target = m_pInterface->Debug_ConvertScreenToWorld(pos);
		}
		break;
	}
	case SDL_KEYDOWN:
	{
		if (e.key.keysym.sym == SDLK_SPACE)
		{
			m_CanRun = true;
		}
		else if (e.key.keysym.sym == SDLK_LEFT)
		{
			m_AngSpeed -= Elite::ToRadians(10);
		}
		else if (e.key.keysym.sym == SDLK_RIGHT)
		{
			m_AngSpeed += Elite::ToRadians(10);
		}
		else if (e.key.keysym.sym == SDLK_g)
		{
			m_GrabItem = true;
		}
		else if (e.key.keysym.sym == SDLK_u)
		{
			m_UseItem = true;
		}
		else if (e.key.keysym.sym == SDLK_r)
		{
			m_RemoveItem = true;
		}
		else if (e.key.keysym.sym == SDLK_d)
		{
			m_DropItem = true;
		}
		break;
	}
	case SDL_KEYUP:
	{
		if (e.key.keysym.sym == SDLK_SPACE)
		{
			m_CanRun = false;
		}
		break;
	}
	}
}

//Update
SteeringPlugin_Output Plugin::UpdateSteering(float dt)
{
	//update info
	m_VecEntityInfo = GetEntitiesInFOV();
	m_VecHouseInfo = GetHousesInFOV();

	//update gun timer (in inventory)
	Inventory* pInventory = nullptr;
	if (m_pBlackboard->GetData(BOARD::INVENTORY, pInventory))
	{
		pInventory->Update(dt);
	}

	//Apply steering
	return m_pBehaviourController->ExecuteBehaviourTree(m_pBlackboard);
}

//This function should only be used for rendering debug elements
void Plugin::Render(float dt) const
{
	//This Render function should only contain calls to Interface->Draw_... functions
	m_pInterface->Draw_SolidCircle(m_Target, .7f, { 0,0 }, { 1, 0, 0 });

	//show waypoints to check inside houses
	vector<Elite::Vector2>* pWaypoints;
	if (m_pBlackboard->GetData(BOARD::WAYPOINTS, pWaypoints))
	{
		std::for_each((*pWaypoints).begin(), (*pWaypoints).end(), [&](const Elite::Vector2 &pos) {
			m_pInterface->Draw_SolidCircle(pos, 1.f, { 0,0 }, Elite::Vector3(0.5f, 0.5f, 0.5f));
		});
	}
	
	//show "Laser sight"
	AgentInfo AI = m_pInterface->Agent_GetInfo();
	float range = AI.FOV_Range / 3;
	m_pInterface->Draw_Segment(AI.Position, AI.Position + Elite::Vector2(range * (float)cos(AI.Orientation - PI/2), range * (float)sin(AI.Orientation - PI/2)), Elite::Vector3(0, 0, 0));
	m_pInterface->Draw_Segment(AI.Position, m_Target, Elite::Vector3(0.5f, 0.5f, 0.5f));
}

vector<HouseInfo> Plugin::GetHousesInFOV() const
{
	vector<HouseInfo> vHousesInFOV = {};

	HouseInfo hi = {};
	for (int i = 0;; ++i)
	{
		if (m_pInterface->Fov_GetHouseByIndex(i, hi))
		{
			vHousesInFOV.push_back(hi);
			continue;
		}

		break;
	}

	return vHousesInFOV;
}

vector<EntityInfo> Plugin::GetEntitiesInFOV() const
{
	vector<EntityInfo> vEntitiesInFOV = {};

	EntityInfo ei = {};
	for (int i = 0;; ++i)
	{
		if (m_pInterface->Fov_GetEntityByIndex(i, ei))
		{
			vEntitiesInFOV.push_back(ei);
			continue;
		}

		break;
	}

	return vEntitiesInFOV;
}
