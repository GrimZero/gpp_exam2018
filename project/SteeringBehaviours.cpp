#include "stdafx.h"
#include "SteeringBehaviours.h"
#include "Blackboard.h"

namespace SteeringBehaviours
{
	SteeringPlugin_Output Seek::CalculateSteering(Blackboard * pBlackboard)
	{
		//create local variables to store
		SteeringPlugin_Output steering = {};
		IExamInterface* pInterface = nullptr;
		Elite::Vector2* pTarget = nullptr;
		bool* pRun = nullptr;

		//gather data from the blackboard
		if (pBlackboard->GetData(BOARD::INTERFACE, pInterface)
			&& pBlackboard->GetData(BOARD::TARGET, pTarget)
			&& pBlackboard->GetData(BOARD::RUNNING, pRun))
		{
			AgentInfo agentInfo = pInterface->Agent_GetInfo();

			//behaviour logic
			Elite::Vector2 targetVelocity = pInterface->NavMesh_GetClosestPathPoint(*pTarget) - agentInfo.Position;
			targetVelocity.Normalize();
			targetVelocity *= agentInfo.MaxLinearSpeed;

			steering.LinearVelocity = targetVelocity - agentInfo.LinearVelocity;
			steering.RunMode = *pRun;
		}
		return steering;
	}

	SteeringPlugin_Output Wander::CalculateSteering(Blackboard * pBlackboard)
	{
		IExamInterface* pInterface = nullptr;
		Elite::Vector2* pTarget = nullptr;
		
		if (pBlackboard->GetData(BOARD::INTERFACE, pInterface) 
			&& pBlackboard->GetData(BOARD::TARGET, pTarget))
		{
			AgentInfo agentInfo = pInterface->Agent_GetInfo();

			Elite::Vector2 offset = agentInfo.LinearVelocity;
			offset.Normalize();
			offset *= m_Offset;

			Elite::Vector2 circleOffset = { cos(m_WanderAngle) * m_Radius, sin(m_WanderAngle) * m_Radius };
			m_WanderAngle += Elite::randomFloat() * m_AngleChange - (m_AngleChange * .5f);

			*pTarget = agentInfo.Position + offset + circleOffset;
		}
		return Seek::CalculateSteering(pBlackboard);
	}
	SteeringPlugin_Output LookAt::CalculateSteering(Blackboard * pBlackboard)
	{
		//create local variables to store
		SteeringPlugin_Output steering = {};
		IExamInterface* pInterface = nullptr;
		Elite::Vector2* pTarget = nullptr;

		//gather data from the blackboard
		if (pBlackboard->GetData(INTERFACE, pInterface) 
			&& pBlackboard->GetData(TARGET, pTarget))
		{
			//angle orient towards target
			AgentInfo AI = pInterface->Agent_GetInfo();
			int range = AI.FOV_Range / 3;

			auto currLookAt = AI.Position + Elite::Vector2(range * (float)cos(AI.Orientation - PI / 2), range * (float)sin(AI.Orientation - PI / 2));
			auto currPos = AI.Position;

			auto currentLookVec = (currLookAt - currPos); currentLookVec.Normalize();
			auto targetLookVec = (*pTarget - currPos); targetLookVec.Normalize();

			auto dot = Elite::Dot(currentLookVec, targetLookVec);
			auto angle = acos(dot);

			steering.AngularVelocity = angle;

			//if looking at the target, is alligned
			bool* pAlligned = nullptr;
			if (pBlackboard->GetData(BOARD::ALLIGNED, pAlligned))
			{
				if (dot < 1.0005f && dot > 0.9995f)
				{
					*pAlligned = true;
				}
				else *pAlligned = false;
			}

			steering.AutoOrientate = false;
		}
		return steering;
	}
}
