#pragma once
#include "stdafx.h"
#include "Singleton.h"

class Logger : public Singleton<Logger>
{

public:
	enum Level
	{
		None,
		Warning, 
		Error
	};

	Logger();
	virtual ~Logger();
	void LogData(Level level, string message);

private:
	void LogWarning(string message);
	void LogError(string message);
};

