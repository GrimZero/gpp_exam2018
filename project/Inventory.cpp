#include "stdafx.h"
#include "Inventory.h"
#include "IExamInterface.h"
#include "Blackboard.h"

Inventory::Inventory(Blackboard* pBlackboard)
{
	pBlackboard->GetData(INTERFACE, m_pData);
}

Inventory::~Inventory()
{
	m_CurrentInventory.clear();
}

void Inventory::Update(float dt)
{
	if (!m_GunCooledDown)
	{
		m_GunCooldown += dt;
	}
	
	if (m_GunCooldown > m_GunMaxCooldown)
	{
		m_GunCooledDown = true;
		m_GunCooldown = 0;
	}
}

const bool Inventory::AddItem(const ItemInfo &item)
{
	for (UINT i = 0; i < m_pData->Inventory_GetCapacity(); i++)
	{
		if (!slotInUse[i])
		{
			slotInUse[i] = true;
			return m_pData->Inventory_AddItem(i, item);
		}
	}
	return false;
}

int Inventory::CheckMetaData(const UINT &slot, const std::string &parameter)
{
	if (slotInUse[slot])
	{
		ItemInfo ie = {};
		m_pData->Inventory_GetItem(slot, ie);
		return (int)m_pData->Item_GetMetadata(ie, parameter);
	}
	return -1;
}

const bool Inventory::RemoveItem(const UINT &slot)
{
	if (slotInUse[slot])
	{
		m_pData->Inventory_RemoveItem(slot);
		slotInUse[slot] = false;
		return true;
	}
	return false;
}

const bool Inventory::UseItem(const UINT &slot)
{
	if (slotInUse[slot])
	{
		ItemInfo ie = {};
		if (m_pData->Inventory_GetItem(slot, ie))
		{
			if (ie.Type == eItemType::PISTOL) m_GunCooledDown = false;
		}
		return m_pData->Inventory_UseItem(slot);
	}
	return false;
}

const bool Inventory::DropItem(const UINT &slot)
{
	if (slotInUse[slot])
	{
		slotInUse[slot] = false;
		return m_pData->Inventory_DropItem(slot);
	}
	return false;
}

const ItemInfo Inventory::GetItem(const UINT &slot)
{
	if (slotInUse[slot])
	{
		ItemInfo ie = {};
		m_pData->Inventory_GetItem(slot, ie);

		return ie;
	}
	return ItemInfo{};
}

vector<Slot>* Inventory::UpdateInventory()
{
	m_CurrentInventory.clear();

	//save items from interface
	for (UINT i = 0; i < m_pData->Inventory_GetCapacity(); i++)
	{
		if (slotInUse[i])
		{
			ItemInfo info;
			m_pData->Inventory_GetItem(i, info);
			m_CurrentInventory.push_back({ i, info });
		}
	}

	//check inventory for empty items
	for (size_t i = 0; i < m_pData->Inventory_GetCapacity(); i++)
	{
		if (slotInUse[i])
		{
			ItemInfo ie = GetItem(i);
			int amount = 0;

			switch (ie.Type)
			{
			case eItemType::PISTOL:
				amount = CheckMetaData(i, "ammo");
				if (amount == 0) RemoveItem(i);
				break;
			case eItemType::MEDKIT:
				amount = CheckMetaData(i, "health");
				if (amount == 0) RemoveItem(i);
				break;
			case eItemType::FOOD:
				amount = CheckMetaData(i, "energy");
				if (amount == 0) RemoveItem(i);
				break;
			case eItemType::GARBAGE:
				RemoveItem(i);
				break;
			}
		}
	}
	return &m_CurrentInventory;
}

vector<Slot>* Inventory::GetInventory()
{
	return &m_CurrentInventory;
}

bool Inventory::GunCooledDown()
{
	return m_GunCooledDown;
}

const UINT Inventory::FindItem(const eItemType &type)
{
	for (size_t i = 0; i < m_CurrentInventory.size(); i++)
	{
		if (slotInUse[i])
		{
			if (m_CurrentInventory[i].item.Type == type)
			{
				return m_CurrentInventory[i].slotNumber;
			}
		}
	}
	return -1;
}

const UINT Inventory::FindItem(const ItemInfo &info)
{
	return FindItem(info.Type);
}