#pragma once

#pragma region //Windows Includes
#include <iostream>
#include <string>
#include <sstream>
#include <math.h>
#include <fstream>
#include <random>
#include <stdio.h>
#include <vector>
#include <list>
#include <queue>
#include <algorithm>
#include <functional>
#include <memory>

//mmgr specific includes
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <stdarg.h>
#include <new>

#ifndef	_WIN32
#include <unistd.h>
#endif

using namespace std;
#pragma endregion

#pragma region //Third-Party Includes
#include <GL/gl3w.h>
#include <ImGui/imgui.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>

#include "EliteMath/EMath.h"
#pragma endregion

//Student side
#include "Logger.h"
#include "Blackboard.h"
#include "Exam_HelperStructsExtension.h"
#define PI 3.14159265f
using namespace BOARD;
