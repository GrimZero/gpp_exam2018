#pragma once
#include "IExamInterface.h"
#include "BehaviorTree.h"

class Blackboard;
class BehaviorTree;
class Inventory;
struct Slot;

class BehaviourManager
{
public:
	BehaviourManager(Blackboard* pBlackboard);
	~BehaviourManager();

	SteeringPlugin_Output ExecuteBehaviourTree(Blackboard* pBlackboard);

private:
	Inventory* m_pInventory = nullptr;
	BehaviorTree* m_pBehaviorTree = nullptr;
	IExamInterface* m_pInterface = nullptr;
	std::vector<Elite::Vector2> m_HouseCheckpoints = {};

	std::vector<Slot> m_Inventory = {};
	bool m_Alligned = false;
	bool m_IsAiming = false;
};